# Simple Weather Progressive Web App
* Includes Manifest and Icon
* Service Workers to be implemented
* Views to be improved

Clone the Project and then do a **npm install** to install the needed modules. Then start the app by **node app.js**!

You can find the online demo [Here](http://weatherseer-weather-app.1d35.starter-us-east-1.openshiftapps.com/).